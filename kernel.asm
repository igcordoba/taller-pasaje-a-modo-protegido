; ** por compatibilidad se omiten tildes **
; ==============================================================================
; TALLER System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
; ==============================================================================

%include "print.mac"

global start


; COMPLETAR - Agreguen declaraciones extern según vayan necesitando

; COMPLETAR - Definan correctamente estas constantes cuando las necesiten
%define CS_RING_0_SEL 0x08;Primer bit 0 GDT, otros 2 bits de privilegios nivel 00, y como la gdt_code esta en el indice 1 entonces el 4 bit en 1
%define DS_RING_0_SEL 0b0000000000011000
%define STACK_INIT 0x25000;


BITS 16
;; Saltear seccion de datos
jmp start

;;
;; Seccion de datos.
;; -------------------------------------------------------------------------- ;;
start_rm_msg db     'Iniciando kernel en Modo Real'
start_rm_len equ    $ - start_rm_msg

start_pm_msg db     'Iniciando kernel en Modo Protegido'
start_pm_len equ    $ - start_pm_msg
;
gdt:
gdt_null_desc dq 0x0000000000000000;
gdt_code_0 dq 0b0000000010000011100110100000000000000000000000000011000100000000
gdt_code_3 dq 0b0000000010000011111110100000000000000000000000000011000100000000
gdt_data_0 dq 0b0000000010000011100100100000000000000000000000000011000100000000
gdt_data_3 dq 0b0000000010000011111100100000000000000000000000000011000100000000
gdt_end:
;
gdtr:
    dw gdt_end - gdt - 1
    dd gdt
gdtr_end:
enable_protected_mode dd 0b00000000000000000000000000010001;
; El bit numero 0 indica que va a pasar a modo real y el bit numero 5 indica Numeric Error
;;
;;
;; Seccion de código.
;; -------------------------------------------------------------------------- ;;

;; Punto de entrada del kernel.
BITS 16
start:
    ; COMPLETAR - Deshabilitar interrupciones
		cli; Instruccion para deshabilitar las interrupciones.

    ; Cambiar modo de video a 80 X 50
    mov ax, 0003h
    int 10h ; set mode 03h
    xor bx, bx
    mov ax, 1112h
    int 10h ; load 8x8 font

    ; COMPLETAR - Imprimir mensaje de bienvenida - MODO REAL
    ; (revisar las funciones definidas en print.mac y los mensajes se encuentran en la
    ; sección de datos)
		print_text_rm	start_rm_msg, start_rm_len, 0x03, 0x00, 0x00; Esto deberia imprimir a partir de la fila 0 y columna 0.

    ; COMPLETAR - Habilitar A20
    ; (revisar las funciones definidas en a20.asm)
		call A20_enable;

    ; COMPLETAR - Cargar la GDT
		lgdt [gdtr]; 	

    ; COMPLETAR - Setear el bit PE del registro CR0
		mov eax, [enable_protected_mode];
		mov cr0, eax;

    ; COMPLETAR - Saltar a modo protegido (far jump)
		jmp CS_RING_0_SEL:modo_protegido; Como estoy definiendo las etiquetas estas, y todo se hace a partir del "0x00", tengo que saltar a la etiqueta de la funcion modo_protegido
    ; (recuerden que un far jmp se especifica como jmp CS_selector:address)
    ; Pueden usar la constante CS_RING_0_SEL definida en este archivo

BITS 32
modo_protegido:
    ; COMPLETAR - A partir de aca, todo el codigo se va a ejectutar en modo protegido
    ; Establecer selectores de segmentos DS, ES, GS, FS y SS en el segmento de datos de nivel 0
    ; Pueden usar la constante DS_RING_0_SEL definida en este archivo
		mov ax, DS_RING_0_SEL;
		mov bx, 0x00;// Pongo esta instruccion porque me saltea esta instruccion ??????? ni idea porque.
    mov fs, ax;
    mov ds, ax         
    mov es, ax
    mov gs, ax
    mov ss, ax

    ; COMPLETAR - Establecer el tope y la base de la pila
		xor ecx, ecx;
		mov ecx, STACK_INIT;
		mov ebp, ecx; Base de la pila.
		mov esp, ecx; Tope de la pila.

    ; COMPLETAR - Imprimir mensaje de bienvenida - MODO PROTEGIDO
		print_text_pm	start_pm_msg, start_pm_len, 0x03, 0x00, 0x00
    ; COMPLETAR - Inicializar pantalla
    
   
    ; Ciclar infinitamente 
    mov eax, 0xFFFF
    mov ebx, 0xFFFF
    mov ecx, 0xFFFF
    mov edx, 0xFFFF
    jmp $

;; -------------------------------------------------------------------------- ;;

%include "a20.asm"
