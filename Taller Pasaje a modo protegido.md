1. Tanto el _modo protegido_ como el _modo real_ hacen referencia a el _modo de operacion del procesador_.
	- _Modo real_: Es el modo en el que arranca una computadora, provee al programador las mismas herramientas (instrucciones) que el procesador _8086_, solamente que con algunas instrucciones mas, como por ejemplo el pasaje a _modo protegido_. El modo opera con _16 bits_ y ademas solo permite direccionar a $2^{20} = 1\text{mb}$  de memoria.
	- _Modo protegido_: Es el modo de operacion _nativo_, en este modo se disponen de todas las instrucciones y caracteristicas, tambien provee la mayor performance y capabilidad 
2. La desventaja que tiene correr tanto _OS_ como _Programas_ en el _modo real_, es que:
	a.Solo opera con _16 bits_ como mencione arriba y ademas solo permite direccionar a _1mb_, ademas de esto solo podria utilizar las _instrucciones_ del procesador _8086_ y no contaria con la _infinidad_ de instruccione que hay en los procesadores nuevo.
3. Al operar en _modo protegido_ (solo?), todas los accesos a memoria pasan por
	a. __GDT__: Tabla de descriptores globales.
	b. __LTD__: Tabla de descriptores locales.
	Como su nombre lo indica, esta tabla contiene _descriptores de segmento_, el cual provee la _direccion base_ del segmento a acceder e _informacion de su uso_. Cada _descriptor de segmento_ a su vez contiene un _selector de segmento_. 
	 Los _descriptores de segmento_ independientemente de si la arquitectura es de _16, 32 o 64_ bits son de _64_ bits.
	 Los primeros _32 bits_ se dividen de la siguiente manera: los _16 bits_ menos significativos _(0:15)_ son el _limite del segmento_ y los _16 bits_ mas significativos son los bits _0:15_ de la _direccion base_. ![[Pasted image 20240518091515.png]]
	 Los segundos _32 bits_ se dividen de la siguiente manera:
	a. _0:7_: Los bits _16:23_ de la _direccion base_.
	b. _8:11_ : Los bits de _Type_ o _Tipo de segmento_.
	c. _12_: _S Descriptor type_, _0_ = system, _1_ = codigo o datos.
	d. _13:14_: _DPL Descriptor privilege level_, se utiliza para aclarar el nivel de privilegios del segmento.
		_00_: _Nivel 0 de privilegio (ring 0)_: Modo _kernel_ o _supervisor_ no tiene restricciones.
		_01_: _Nivel 1 de privilegio (ring 1)_: Modo intermedio.
		_10_: _Nivel 2 de privilegio_: Modo intermedio.
		_11_: _Nivel 3 de privilegio_: Modo _usuario_, ya hay restricciones tanto al _hardware_ como a ciertas areas del _sistema_.
	e. _15_: _P present_ , se utiliza para aclarar si el segmento esta presente en memoria o esta en el area de _swap_.
	f. _16:19_: _Segment limit_: 4 bits mas que se utilizan para extender el limite del segmento.
	g. _20_: _AVL Avilable to use by system software_ reservado sin ningun proposito para el hardware.
	h. _21_: _0_, bit seteado en 0 por INTEL.
	g. _22_: _D/B Default operation size_, se utiliza para decir si el segmento es de _0_: _16 bits_, _1_: _32 bits_.
	i: _23_: _G Granularity_, se utiliza para decir en que unidad esta expresado el limite del segmento.
		_0_: Se expresa en _unidades de bytes_, por lo tanto como maximo puedo direccionar a $2^{20} \cdot 1\text{byte} = 1\text{mb}$. Osea seria _limite de segmento * 1byte_
		_1_: Se expresa en _unidades de 4Kbytes_ por lo tanto como maximo puedo direccionar a $2^{20} \cdot 1\text{kb} = 4\text{gb}$. Osea seria _limite del segmento * 4kbytes_.
	h: _24:32_: los bits _24:32_ de la _dreccion base_.
4. Los bits que tendriamos que setearn en _Type_ para poder especificar un _segmento de ejecucion y lectura de codigo son:_ __1010__.
5. https://docs.google.com/spreadsheets/d/1N9epCUN_N24TbHObvNTKbDOxuHs7h689hHxHhpt97Sw/edit#gid=0
6. El _gdt_descriptor_t_ describe a donde va a estar la tabla _GDT_ y tambien cual va a ser su 'tamano', mientras que el _gdt_entry_t_ es una estructura que describe un _descriptor de segmento_. 
7. _Registros de segmento_: Son registros de _16 bits_ que se usan para seleccionar un _segmento_, los mas importantes son:
	_CS_: Code Segment
	_DS_: Data Segment
	_SS_: Stack Segment
	Estos registros estan divididos de la siguiente forma:
	_0:2_: Los primeros 3 bits se usan como bits de control:
		_0_: el primer bit se utiliza para saber si apuntar a la _GDT_ o a la _LDT_, 
		_1:2_: se utilizan para determinar los niveles de privilegio.
	_3:15_: Los ultimos 13 bits se usan para saber cual es el indice del _descriptor_ dentro de la tabla de descriptores correspondiente (_GDT o LDT_).
8. -
9. La instruccion _cli_ desactiva las interrupciones seteando el bit _IF_ en 0.
10. La instruccion _LGDT_ carga en el registro _GDTR_ la direccion de memoria en la que va a estar la _GDT_. Si el operando es de _32 bits_ utiliza los primeros _16_ para setear el limite de la _GDT_ y los otros _32_ son donde empieza la _GDT_. Esta instruccion en _modo real_ solo toma una direccion de memoria que es el comienzo de la _GDT_ y por defecto ocupa _64kbs_.
	La estructura que lo indica es la _gdt descriptor t_ y se inicializa en la linea __99__ del _gdt.c_.
11. - 
12. -
\divider
13. Para poder pasar a _modo protegido_ hay que activar el bit _PE_ de el resgistro de control _CR0_,  una vez que el _modo protegido_ fue activado ya no hay forma de volver a modo real.![[Pasted image 20240518171236.png]]
	_PE_: Protected Enable, habilita el modo protegido.
	En _modo protegido_ estos registos pueden ser escritos/leidos con la instruccion _MOV_.
14. Hay 4 tipos de _JMPs_ pero para poder posicionarse en el _segmento de codigo protegido_ hay que hacer un _far jump_, este se especifica como _jmp selector_segmento:offset_dentro_segmento_.